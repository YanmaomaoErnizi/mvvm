class Dep {
    constructor() {
        this.events = []
    }
    pushEvents(watcher) {
        this.events.push(watcher)
    }
    handleEvent() {
        this.events.forEach(item => {
            item.sendVal()
        })
    }
}
// 设置一个静态属性
Dep.target = null;
let dep = new Dep()

// 数据对象添加劫持的类
class Observe {
    constructor(data) {
        // 判断数据是不是引用数据类型 如果是接着往下设置劫持  如果不是直接return
        if (typeof data !== 'object' || data == '') {
            return;
        }
        this.data = data;
        this.init()
    }
    init() {
        Object.keys(this.data).forEach(key => {
            this.observe(this.data, key, this.data[key])
        })
    }
    observe(target, key, val) {
        // 通过递归实现每个属性的数据劫持
        new Observe(target[key])
        Object.defineProperty(target, key, {
            get() {
                if (Dep.target) {
                    dep.pushEvents(Dep.target)
                }
                return val
            },
            set(newVal) {
                if (newVal === val) {
                    return
                }
                val = newVal

                // 为了兼容新值为一个对象的时候，该对象的属性也得添加劫持
                new Observe(val)
                dep.handleEvent()
            }
        })
    }
}

class watcher {
    constructor(data, key, cbk) {
        this.data = data
        this.key = key
        this.cbk = cbk
        // 每一次实例watcher的时候，均会把当前实例赋值给Dep的target静态属性
        Dep.target = this
        this.init()
    }
    init() {
        // 获取对应的value
        let value = utils.getDataValue(this.key, this.data)
        Dep.target = null
        return value;
    }
    sendVal() {
        let newVal = this.init()
        this.cbk(newVal)
    }
}

const utils = {
    // 设置内容
    setVal(node, key, data, value) {
        node[value] = this.getDataValue(key, data)
    },
    getDataValue(key, data) {
        if (key.indexOf('.') > -1) {
            let arr = key.split('.')
            arr.forEach((item) => {
                data = data[item]
            })
            return data
        } else {
            return data[key]
        }
    },
    changeData(value, data, key) {
        if (key.indexOf('.') > -1) {
            let arr = key.split('.') // [a, b, c]
            for (var i = 0; i < arr.length - 1; i++) {
                // a.b.c  要去改变c的值  先要取到b
                data = data[arr[i]]
            }
            // 赋值
            data[arr[arr.length - 1]] = value
        } else {
            data[key] = value
        }
    }
}

class mvvm {
    constructor({ el, data }) {
        // 根结点
        this.$el = document.getElementById(el);
        // 所有的数据
        this.$data = data;
        // 将当前数据对象添加劫持
        new Observe(this.$data)
        // 节点属性转换
        this.initDom()
    }
    // 节点属性转换
    initDom() {
        // 文本碎片--> 避免因为操作DOM而导致浏览器的多次重绘（操作完成之后可把整个碎片添加进去，浏览器课一并识别渲染）
        let newFargment = this.createFragment();
        // 根据nodeType来替换对应的属性值
        this.setDomVal(newFargment)
        this.$el.appendChild(newFargment);
    }

    // 设置文本属性
    setDomVal(fragment) {
        // 判断节点类型
        if (fragment.nodeType == 1) {
            let attributes = fragment.attributes
            let isInput = attributes && attributes.length > 0 && [...attributes].filter((item) => {
                return item.nodeName === 'v-model'
            })
            if (isInput.length > 0) {
                let key = isInput[0].nodeValue // defaultValue
                // 给input添加事件
                fragment.addEventListener('input', (e) => {
                    let changeValue = e.target.value
                    // 改变this.$data的值
                    utils.changeData(changeValue, this.$data, key)
                })
                // 把data的默认值赋值到input.values
                utils.setVal(fragment, key, this.$data, 'value')
            }
        } else if (fragment.nodeType == 3) {
            // 文本节点
            if (fragment.nodeValue.indexOf('{{') > -1) {
                let key = fragment.nodeValue.split("{{")[1].split("}}")[0]
                // 改变{{defaultValue}}的值
                new watcher(this.$data, key, (newVal) => {
                    fragment.textContent = newVal
                })
                // 把data的默认值赋值给{{defaultValue}}
                utils.setVal(fragment, key, this.$data, 'textContent')
            }
        }
        if (fragment.childNodes && fragment.childNodes.length > 0) {
            fragment.childNodes.forEach((item) => {
                this.setDomVal(item)
            })
        }
    }

    createFragment() {
        let fragment = document.createDocumentFragment();
        let firstChild;
        while (firstChild = this.$el.firstChild) {
            fragment.appendChild(firstChild)
        }
        return fragment;
    }

}
